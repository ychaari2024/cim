FROM ubuntu:20.04

RUN apt-get -qq update \
    && apt-get -qq install -y --no-install-recommends --no-install-suggests -o=Dpkg::Use-Pty=0 \
        git python3-pip ca-certificates g++-9 gcc-9 build-essential cmake pkg-config curl openjdk-8-jdk \
        gpg wget nano lcov libgtest-dev valgrind libpcsclite-dev libxml2-dev libxml2-utils libssl-dev \
        autoconf libtool supervisor net-tools iputils-ping netcat tcpdump ninja-build m4 libcurl4-openssl-dev \
        # Clean apt cache
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-9 100 \
    && update-alternatives --install /usr/bin/c++ c++ /usr/bin/g++-9 100 \
    && update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 100 \
    && update-alternatives --install /usr/bin/cc cc /usr/bin/gcc-9 100 \
    && update-alternatives --install /usr/bin/gcov gcov /usr/bin/gcov-9 100 \
    && update-alternatives --install /usr/bin/gcov-dump gcov-dump /usr/bin/gcov-dump-9 100 \
    && update-alternatives --install /usr/bin/gcov-tool gcov-tool /usr/bin/gcov-tool-9 100

RUN wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | tee /usr/share/keyrings/kitware-archive-keyring.gpg >/dev/null \
    && echo 'deb [signed-by=/usr/share/keyrings/kitware-archive-keyring.gpg] https://apt.kitware.com/ubuntu/ focal main' | tee /etc/apt/sources.list.d/kitware.list >/dev/null \
    && apt-get update \
    && apt-get -qq install -y --no-install-recommends --no-install-suggests -o=Dpkg::Use-Pty=0 cmake=3.22.* cmake-data=3.22.*

# Install bison
RUN wget https://ftp.gnu.org/gnu/bison/bison-3.0.5.tar.gz \
    && tar -xvf bison-3.0.5.tar.gz \
    && rm -rf bison-3.0.5.tar.gz \
    && cd bison-3.0.5 \
    && ./configure \
    && make \
    && make install

# Install conan
RUN pip install conan==1.62.0
# Set WORKSPACE environment variable
ENV WORKSPACE /root/workspace
# Create WORKSPACE folder
RUN mkdir $WORKSPACE

######################################################
# DEPRECATED: library installation
######################################################

# Build & install grpc and protoc, inspired from grpc distrib build test:
# https://github.com/grpc/grpc/tree/master/test/distrib/cpp/run_distrib_test_cmake.sh
# This cmake method is required to benefit from cmake find_package capability
RUN ( \
    cd ${WORKSPACE} && \
    git clone -b v1.26.x --depth=1 https://github.com/grpc/grpc && \
    cd grpc && \
    git submodule update --init --depth=1 && \
    # Install c-ares
    cd third_party/cares/cares && \
    git fetch origin && \
    git checkout cares-1_15_0 && \
    mkdir -p cmake/build && \
    cd cmake/build && \
    cmake -DCMAKE_BUILD_TYPE=Release ../.. && \
    make -j8 && \
    make -j8 install && \
    cd ${WORKSPACE}/grpc && \
    # wipe out to prevent influencing the grpc build
    rm -rf third_party/cares/cares && \
    # Install zlib
    cd third_party/zlib && \
    mkdir -p cmake/build && \
    cd cmake/build && \
    cmake -DCMAKE_BUILD_TYPE=Release ../.. && \
    make -j8 && \
    make -j8 install && \
    cd ${WORKSPACE}/grpc && \
    # wipe out to prevent influencing the grpc build
    rm -rf third_party/zlib && \
    # Install protobuf
    cd third_party/protobuf && \
    mkdir -p cmake/build && \
    cd cmake/build && \
    cmake -Dprotobuf_BUILD_TESTS=OFF -DCMAKE_BUILD_TYPE=Release ../ && \
    make -j8 && \
    make -j8 install && \
    cd ${WORKSPACE}/grpc && \
    # wipe out to prevent influencing the grpc build
    rm -rf third_party/protobuf && \
    # Install gRPC
    mkdir -p cmake/build && \
    cd cmake/build && \
    cmake -DgRPC_INSTALL=ON -DgRPC_BUILD_TESTS=OFF -DgRPC_PROTOBUF_PROVIDER=package -DgRPC_ZLIB_PROVIDER=package -DgRPC_CARES_PROVIDER=package -DgRPC_SSL_PROVIDER=package -DCMAKE_BUILD_TYPE=Release ../.. && \
    make -j8 && \
    make -j8 install && \
    rm -rf $WORKSPACE/grpc \
)

# Install GTest library
RUN cd /usr/src/googletest && \
    cmake . && \
    cmake --build . --target install

# Dependencies
COPY ubuntu-20-deps.tgz /opt

RUN cd /opt \
    && tar xf ubuntu-20-deps.tgz \
    && rm ubuntu-20-deps.tgz \
    && cp boost-debs/boost.list /etc/apt/sources.list.d/ \
    && apt update \
    && apt install libboost-atomic1.65-dev:amd64 libboost-atomic1.65.1:amd64 libboost-chrono1.65-dev:amd64 libboost-chrono1.65.1:amd64 libboost-date-time1.65-dev:amd64 libboost-date-time1.65.1:amd64 libboost-filesystem1.65-dev:amd64 libboost-filesystem1.65.1:amd64 libboost-log1.65-dev libboost-log1.65.1 libboost-regex1.65.1:amd64 libboost-serialization1.65-dev:amd64 libboost-serialization1.65.1:amd64 libboost-system1.65-dev:amd64 libboost-system1.65.1:amd64 libboost-thread1.65-dev:amd64 libboost-thread1.65.1:amd64 libboost1.65-dev:amd64

ENV PATH=$PATH:/opt/capicxx-core-tools:/opt/capicxx-someip-tools
